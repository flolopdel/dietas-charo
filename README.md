# Phonegap + Cordova + Vuejs + F7

Example of a phonegap app working with VueJs https://v1.framework7.io/vue/

MODELS;COLLECTION http://vuemc.io/#introduction

### Installing and run dev environment

```
docker-compose up
```
Your application is running here: http://localhost:8080

### Installing and create debug apk
```
cd path/to/project
docker-compose -f docker-compose-apk.yml up -d --build
docker-compose -f docker-compose-apk.yml ps (check the environment is healthy)
docker-compose -f docker-compose-apk.yml exec node_apk bash
```
Inside the container
```
npm install
cordova platform add android
npm run cordova-build-android
```

### Generate and sing keystore (optional, only if it is required)
```
keytool -genkey -v -keystore baseproject.keystore -alias baseproject_app_release -validity 20000
```

### Get hash to facebook (optional, only if it is required)
```
keytool -exportcert -alias baseproject_app_release -keystore "baseproject.keystore" | openssl sha1 -binary | openssl base64
```