/* eslint-disable no-unused-vars */

import Vue from 'vue';

// Import F7
import Framework7 from 'framework7/dist/framework7.esm.bundle.js';

// Import F7 Vue Plugin
import Framework7Vue from 'framework7-vue/dist/framework7-vue.esm.bundle.js';

// Import F7 Styles
import Framework7Styles from 'framework7/dist/css/framework7.css';

import OfferOverview from '@/pages/offer/offer-overview';

// Init F7 Vue Plugin
Vue.use(Framework7Vue, Framework7);

let vm;

describe('offer-overview.vue', () => {
  beforeEach(() => {
    vm = new Vue({
      // eslint-disable-line no-new
      el: document.createElement('div'),
      render: h => h(OfferOverview),
      // Init Framework7 by passing parameters here
      // The absolute minimum is an empty routes array
      framework7: {
        routes: []
      }
    });
  });

  describe('Data', () => {
    it('should have a name of "offer-overview"', () => {
      expect(OfferOverview.name).to.equal('offer-overview');
    });

    it('should have a data method', () => {
      expect(OfferOverview.data).to.be.a('function');
    });

    it('should have a data method that returns a title of "offer-overview"', () => {
      expect(OfferOverview.data().title).to.equal('Offers');
    });

    it('The nav bar title should match with `data().title`', () => {
      expect(vm.$el.querySelector('.navbar-inner .title').textContent).to.equal(
        OfferOverview.data().title
      );
    });
  });
});
