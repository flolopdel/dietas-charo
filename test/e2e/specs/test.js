// For authoring Nightwatch tests, see
// http://nightwatchjs.org/guide#usage

module.exports = {
  'main view': browser => {
    // automatically uses dev Server port from /config.index.js
    // default: http://localhost:8080
    // see nightwatch.conf.js
    const devServer = browser.globals.devServerURL;

    browser
      .url(devServer)
      .waitForElementVisible('#app', 5000)
      .assert.elementPresent('#main-view');
  },
  'nav bar': browser => {
    const devServer = browser.globals.devServerURL;

    browser
      .url(devServer)
      .waitForElementVisible('.navbar-inner .title', 5000)
      .assert.containsText('.navbar-inner .title', 'Offers');
  },
  'content block': browser => {
    const devServer = browser.globals.devServerURL;

    browser
      .url(devServer)
      .waitForElementVisible('item-title-row', 5000)
      .assert.containsText(
        '.item-title-row',
        'ambassadeur vrijwilligerswerk'
      )
      .end();
  }
};
