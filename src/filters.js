/* eslint-disable no-unused-vars */
// Import Vue
import Vue from 'vue';

import moment from 'moment'

Vue.filter('formatDateTime', function(value) {
  if (value) {
    return moment(String(value)).format('DD-MM-YYYY HH:mm:ss')
  }
});

Vue.filter('formatDate', function(value) {
  if (value) {
    return moment(String(value)).format('DD-MM-YYYY')
  }
});
Vue.filter('formatLongDate', function(value) {
  if (value) {
    return moment(String(value)).format('Do MMM YYYY')
  }
});

Vue.filter('formatLongDateTime', function(value) {
  if (value) {
    return moment(String(value)).format('Do MMM YYYY HH:mm')
  }
});