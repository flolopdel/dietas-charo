import NotFoundPage from './pages/not-found.vue';
import Overview from './pages/overview.vue';

export default [
  /*{
    path: '/',
    component: SectionOverview
  },
  */
  {
    path: '/',
    component: Overview
  },
  {
    path: '(.*)',
    component: NotFoundPage
  }
];

