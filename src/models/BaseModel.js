import { Model } from 'vue-mc'
import Vue from 'vue'

export default class BaseModel extends Model {

    constructor(attributes = {}, collection = null, options = {}) {
        super(attributes, collection, options);
        this.setOption('methods.update', 'PUT');

        Vue.set(this, 'token', localStorage.getItem('device_token') || "T00mb44cc3ss2018");

    }

    /**
     * Change general request
     */
    getRequest(config) {
        config.baseURL = process.env.API_BASE_URL;
        return super.getRequest(config);

    }

    /**
     * @returns {Object} Headers to use in all request.
     */
    getDefaultHeaders() {

        return {
            'X-API-KEY': JSON.parse(localStorage.getItem('f7form-login')).token || "",
            'Content-Type': 'multipart/form-data;'
        };
    }

    /**
     * Called when a fetch request was successful.
     */
    onFetchSuccess(response) {

        let attributes = response.getData().data;
        // A fetch request must receive *some* data in return.
        if (_.isEmpty(attributes)) {
            throw new ResponseError("No data in fetch response", response);
        }

        this.assign(attributes);

        Vue.set(this, 'fatal',   false);
        Vue.set(this, 'loading', false);

        this.emit('fetch', {error: null});
    }

    /**
     * Called when a save request was successful.
     *
     * @param {Object|null} response
     */
    onSaveSuccess(response) {

        // Clear errors because the request was successful.
        this.clearErrors();

        // Update this model with the data that was returned in the response.
        if (response) {
            let model  = response.getData().data;
            this.update(model);
        }

        Vue.set(this, 'saving', false);
        Vue.set(this, 'fatal',  false);

        // Automatically add to all registered collections.
        this.addToAllCollections();

        this.emit('save', {error: null});
    }

    onSaveValidationFailure(error) {
        super.onSaveValidationFailure(error);
        this.setErrors(error.response.getData().data);
    }

    /**
     * @returns {Object} The data to send to the server when saving this model.
     */
    getSaveData() {
        // Only use changed attributes if patching.
        if (this.isExisting() && this.shouldPatch()) {
            return _.pick(this._attributes, this.changed());
        }

        var form_data = new FormData();

        for ( var key in this._attributes ) {
            form_data.append(key, this._attributes[key]);
        }

        return form_data;
    }

    getError(value) {
        var errors = this.getErrors();

        if(Object.keys(errors).length){
            if (errors[value]){
                return errors[value];
            }
        }
        return false;
    }
}