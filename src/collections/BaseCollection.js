import { Collection } from 'vue-mc'
import _ from 'lodash'

export default class BaseCollection extends Collection {


    /**
     * Change general request
     */
    getRequest(config) {
        config.baseURL = "https://europe-west1-second-modem-265017.cloudfunctions.net/";
        return super.getRequest(config);

    }
}