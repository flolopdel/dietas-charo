import BaseCollection from './BaseCollection'

export default class LunchCollection extends BaseCollection {

  routes() {
    return {
      fetch: 'food?type=lunch'
    }
  }

}