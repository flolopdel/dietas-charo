import BaseCollection from './BaseCollection'

export default class DinnerCollection extends BaseCollection {

  routes() {
    return {
      fetch: 'food?type=dinner',
    }
  }

}