export const _lunchs = [
  {
    'id':1,
    'name': 'Macarrones con carne picada',
    'tags': 'pasta,carnes',
    'relations': [1,2,3]
  },
  {
    'id':2,
    'name': 'Tortilla calabacÃ­n y sopa de pollo',
    'tags': 'huevo',
    'relations': [4]
  },
  {
    'id':3,
    'name': 'Huevo Planca con Pisto',
    'tags': 'huevo',
    'relations': [5,7]
  },
  {
    'id':4,
    'name': 'Tortilla de verduras (2 huevos) y Caldo de Pollo',
    'tags': 'huevo',
    'relations': [6,3,8,9]
  },
  {
    'id':5,
    'name': 'Sopa de pollo y tortilla de JamÃ³n',
    'tags': 'huevo',
    'relations': [7]
  },
  {
    'id':6,
    'name': 'Arroz con pollo y Ensalada',
    'tags': 'arroz',
    'relations': [10]
  },
  {
    'id':7,
    'name': 'Paella y Ensalada',
    'tags': 'arroz',
    'relations': [11,12,13]
  },
  {
    'id':8,
    'name': 'Lentejas con verduras y ensalada de lechuga, zanahorias y maiz',
    'tags': 'legumbres',
    'relations': [14]
  },
  {
    'id':9,
    'name': 'Cocido de Garbanzos y ensalada lechuga con tomate',
    'tags': 'legumbres',
    'relations': [9,15]
  },
  {
    'id':10,
    'name': 'Lomo a la plancha. Ensalada lechuga y tomate',
    'tags': 'carnes',
    'relations': [16]
  },
  {
    'id':11,
    'name': 'Lomo a la plancha. ChampiÃ±on con JamÃ³n',
    'tags': 'carnes',
    'relations': [11]
  },
  {
    'id':12,
    'name': 'Lomo a la plancha. Alcachofas con JamÃ³n',
    'tags': 'carnes',
    'relations': [17]
  },
  {
    'id':13,
    'name': 'Lomo a la pimienta. Ensalada de lechuga',
    'tags': 'carnes',
    'relations': [18]
  },
  {
    'id':14,
    'name': 'Terneara a la plancha. Espinacas con JamÃ³n',
    'tags': 'carnes',
    'relations': [2,19]
  },
  {
    'id':15,
    'name': 'Ternera a la plancha. JudÃ­as verdes con Patatas.',
    'tags': 'carnes',
    'relations': [5]
  },
  {
    'id':16,
    'name': 'Pollo asado. Sopa de pollo.',
    'tags': 'carnes',
    'relations': [20]
  },
  {
    'id':17,
    'name': 'Pechuga de pavo a la plancha. CalabacÃ­n a la plancha.',
    'tags': 'carnes',
    'relations': [20]
  },
  {
    'id':18,
    'name': 'Pechuga de pollo a la plancha. Ensalada de canÃ³nigos.',
    'tags': 'carnes',
    'relations': [21]
  },
  {
    'id':19,
    'name': 'Pollo asado. Setas a la plancha.',
    'tags': 'carnes',
    'relations': [22]
  },
  {
    'id':20,
    'name': 'Pollo a la jardinera. Ensalada de lechuga y pepino.',
    'tags': 'carnes',
    'relations': [3]
  },
  {
    'id':21,
    'name': 'Pollo asado y Ensalada lechuga, tomate, zanahorias y cebolleta',
    'tags': 'carnes',
    'relations': [23]
  },
  {
    'id':22,
    'name': 'Pollo en salsa ligera. Sopa de pollo.',
    'tags': 'carnes',
    'relations': [24]
  },
  {
    'id':23,
    'name': 'Pechuga a la plancha. Crema de calabacÃ­n.',
    'tags': 'carnes',
    'relations': [3]
  },
  {
    'id':24,
    'name': 'Pollo al limÃ³n. Ensalada de lechuga y zanahoria.',
    'tags': 'carnes',
    'relations': [5]
  },
  {
    'id':25,
    'name': 'Pollo a la jardinera. Ensalada lechuga, cebolleta y maiz.',
    'tags': 'carnes',
    'relations': [1]
  },
  {
    'id':26,
    'name': 'Pollo asado. EspÃ¡rragos con jamÃ³n.',
    'tags': 'carnes',
    'relations': [1]
  },
  {
    'id':27,
    'name': 'Pollo al ajillo y ensalada de lechuga.',
    'tags': 'carnes',
    'relations': [25]
  },
  {
    'id':28,
    'name': 'Pollo asado y Alcachofas al ajillo.',
    'tags': 'carnes',
    'relations': [8]
  },
  {
    'id':29,
    'name': 'Pollo encebollado y Ensalada de lechuga.',
    'tags': 'carnes',
    'relations': [26]
  },
  {
    'id':30,
    'name': 'Pechuga a la plancha y pisto.',
    'tags': 'carnes',
    'relations': [1]
  },
  {
    'id':31,
    'name': 'SalmÃ³n a la plancha. Ensalada de canÃ³nigos, maiz dulce y zanahoria rayada.',
    'tags': 'pescados',
    'relations': [27]
  },
  {
    'id':32,
    'name': 'Merluza a la plancha. Tomate picado y espÃ¡rragos a la plancha.',
    'tags': 'pescados',
    'relations': [9]
  },
  {
    'id':33,
    'name': 'Pez espada con gambas y Sopa de pollo.',
    'tags': 'pescados',
    'relations': [28]
  },
  {
    'id':34,
    'name': 'Calamares a la plancha, gambas a la plancha y Ensalada.',
    'tags': 'pescados',
    'relations': [29]
  },
  {
    'id':35,
    'name': 'Aguja a la plancha y Seta a la plancha.',
    'tags': 'pescados',
    'relations': [9]
  },
  {
    'id':36,
    'name': 'SalmÃ³n a la plancha y crema de calabacÃ­n',
    'tags': 'pescados',
    'relations': [28]
  },
  {
    'id':37,
    'name': 'Aguja a la plancha y Alcachofas con JamÃ³n.',
    'tags': 'pescados',
    'relations': [3]
  },
  {
    'id':38,
    'name': 'SalmÃ³n a la plancha, almejas al vapor y Ensalada.',
    'tags': 'pescados',
    'relations': [30]
  },
  {
    'id':39,
    'name': 'Bacalao a la plancha. Ensalada de lechuga, zanahoria y EspÃ¡rragos blancos.',
    'tags': 'pescados',
    'relations': [31]
  },
  {
    'id':40,
    'name': 'Merluza a la plancha. Crema de calabacÃ­n. Tomate picado',
    'tags': 'pescados',
    'relations': [19]
  },
  {
    'id':41,
    'name': 'Almejas al vapor. Aguja plancha. Ensalada de lechuga.',
    'tags': 'pescados',
    'relations': [28]
  },
  {
    'id':42,
    'name': 'Bacalao con tomate. Ensalada de lechuga y atÃºn.',
    'tags': 'pescados',
    'relations': [7]
  },
  {
    'id':43,
    'name': 'Calamares a la plancha. Ensalada de lechuga tomate y zanahoria.',
    'tags': 'pescados',
    'relations': [32]
  },
  {
    'id':44,
    'name': 'Aguja a la plancha. Alcachofas al ajillo.',
    'tags': 'pescados',
    'relations': [33]
  }
];

export const _dinners = [
  {
    'id':1,
    'name': 'Ensalada Tomate, AtÃºn y espÃ¡rragos blancos',
    'tags': 'pasta',
    'relations': [1,25,26,30]
  },
  {
    'id':2,
    'name': 'Sopa de Pollo y JamÃ³n Serrano',
    'tags': 'pasta',
    'relations': [1,14]
  },
  {
    'id':3,
    'name': 'Tortilla Francesa y Ensalada',
    'tags': 'pasta',
    'relations': [1,20,23,37]
  },
  {
    'id':4,
    'name': 'Parrillada de verduras y JamÃ³n Serrano',
    'tags': 'huevo',
    'relations': [2]
  },
  {
    'id':5,
    'name': 'ChampiÃ±Ã³n con JamÃ³n',
    'tags': 'huevo',
    'relations': [3,15,24]
  },
  {
    'id':6,
    'name': 'Ensalada de JudÃ­as Verdes con Tomate, atÃºn, Huevo duro y zanahorias',
    'tags': 'huevo',
    'relations': [4]
  },
  {
    'id':7,
    'name': 'Pechuga de Pavo plancha y Ensalada',
    'tags': 'huevo',
    'relations': [3,4,5,42]
  },
  {
    'id':8,
    'name': 'CalabacÃ­n Plancha y JamÃ³n Serrano',
    'tags': 'huevo',
    'relations': [4,28]
  },
  {
    'id':9,
    'name': 'Ensalada huevo duro y piÃ±a',
    'tags': 'huevo',
    'relations': [4,32,35]
  },
  {
    'id':10,
    'name': 'Pechuga de Pavo plancha y tomate picado',
    'tags': 'arroz',
    'relations': [6]
  },
  {
    'id':11,
    'name': 'Ensalada de tomate y atÃºn',
    'tags': 'arroz',
    'relations': [7]
  },
  {
    'id':12,
    'name': 'Pechuga de pavo plancha',
    'tags': 'arroz',
    'relations': [7]
  },
  {
    'id':13,
    'name': 'Gazpacho y jamÃ³n serrano',
    'tags': 'arroz',
    'relations': [7]
  },
  {
    'id':14,
    'name': 'Sopa de pollo con fideos y pechuga de pavo',
    'tags': 'legumbres',
    'relations': [8]
  },
  {
    'id':15,
    'name': 'Tortilla de champiÃ±Ã³n',
    'tags': 'legumbres',
    'relations': [9]
  },
  {
    'id':16,
    'name': 'Ensalada de lechuga, huevo duro, palitos de mar y piÃ±a',
    'tags': 'carnes',
    'relations': [10]
  },
  {
    'id':17,
    'name': 'Ensalada JudÃ­as verdes',
    'tags': 'carnes',
    'relations': [12]
  },
  {
    'id':18,
    'name': 'Hamburguesa de pollo. Verdura a la plancha.',
    'tags': 'carnes',
    'relations': [13]
  },
  {
    'id':19,
    'name': 'Crema de calabacÃ­n. Queso fresco.',
    'tags': 'carnes',
    'relations': [14, 40]
  },
  {
    'id':20,
    'name': 'EspÃ¡rragos a la plancha con JamÃ³n',
    'tags': 'carnes',
    'relations': [16,17]
  },
  {
    'id':21,
    'name': 'Sopa de pescado. JamÃ³n serrano.',
    'tags': 'pescado',
    'relations': [18]
  },
  {
    'id':22,
    'name': 'Ensalada con pollo.',
    'tags': 'pescado',
    'relations': [19]
  },
  {
    'id':23,
    'name': 'Alcachofas con jamÃ³n.',
    'tags': 'pescado',
    'relations': [21]
  },
  {
    'id':24,
    'name': 'Hamburguesa de pollo.',
    'tags': 'pescado',
    'relations': [22]
  },
  {
    'id':25,
    'name': 'Pechuga de pavo o fiambre.',
    'tags': 'pescado',
    'relations': [27]
  },
  {
    'id':26,
    'name': 'Setas a la plancha y JamÃ³n.',
    'tags': 'pescado',
    'relations': [29]
  },
  {
    'id':27,
    'name': 'Pechuga de pollo a la plancha y Ensaldad de lechuga.',
    'tags': 'pescado',
    'relations': [31]
  },
  {
    'id':28,
    'name': 'Hamburguesa de pollo y Ensalada.',
    'tags': 'carnes',
    'relations': [33,36,41]
  },
  {
    'id':29,
    'name': 'Salteado de verduras con pollo.',
    'tags': 'carnes',
    'relations': [34]
  },
  {
    'id':30,
    'name': 'Hamburguesa de pollo. Ensalada de lechuga y zanahoria',
    'tags': 'carnes',
    'relations': [38]
  },
  {
    'id':31,
    'name': 'Ensalada de lechuga y queso fresco.',
    'tags': 'carnes',
    'relations': [39]
  },
  {
    'id':32,
    'name': 'Pechuga de pavo. Sopa de pollo.',
    'tags': 'carnes',
    'relations': [43]
  },
  {
    'id':33,
    'name': 'JudÃ­as verdes con tomate y atÃºn.',
    'tags': 'carnes',
    'relations': [44]
  }
];