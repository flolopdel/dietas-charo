FROM azabost/android-sdk-28

ENV PATH=$PATH:/opt/gradle/gradle-5.6/bin
ENV NODE_VERSION=10
ENV HOME=/usr/src/app

# Java installed?
RUN java -version

# Install NodeJS and NPM
RUN curl -sL https://deb.nodesource.com/setup_$NODE_VERSION.x | bash - && \
    apt-get install nodejs && \
    curl -L https://npmjs.org/install.sh | sh && \
    node -v

# Install Cordova
RUN npm install --global cordova

# Install Gradle
RUN wget https://services.gradle.org/distributions/gradle-5.6-all.zip -q -O /tmp/gradle-5.6-bin.zip && \
    mkdir /opt/gradle && \
    unzip -q -d /opt/gradle /tmp/gradle-5.6-bin.zip

# Create app dir
RUN mkdir -p $HOME && \
    cd $HOME && \
    cordova telemetry off

COPY . $HOME

WORKDIR $HOME

CMD cordova requirements android
